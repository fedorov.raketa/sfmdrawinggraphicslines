CREATE TABLE Kits (
    KitID INTEGER PRIMARY KEY AUTOINCREMENT
                   UNIQUE
                   NOT NULL,
    Name   VARCHAR NOT NULL,
    X      INTEGER NOT NULL,
    Y      INTEGER NOT NULL,
    Color  INTEGER NOT NULL
);