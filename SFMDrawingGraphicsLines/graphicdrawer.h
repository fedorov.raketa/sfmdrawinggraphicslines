#ifndef GRAPHICDRAWER_H
#define GRAPHICDRAWER_H

#include <QFont>
#include "qcustomplot.h"

class GraphicDrawer
{
public:
    GraphicDrawer(QCustomPlot* customPlotter);
    void DrawGraphic(QVector<double>& listX, QVector<double>& listY);
    void DrawLegend();

    void SetKitName(const QString &value);
    void SetGraphicColor(QColor color);

private:
    QCustomPlot* plot;
    QString kitName;
    QColor color;

    void SetMarkerStyle();
};

#endif // GRAPHICDRAWER_H
