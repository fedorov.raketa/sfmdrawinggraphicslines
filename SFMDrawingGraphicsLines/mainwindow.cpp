#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->removeTab(0);

    dbManager = DataBaseManager::getInstance();
    initCoordinatesTabs();

    QPushButton* addTabPushButton = new QPushButton("+");
    //addTabPushButton->setFixedWidth(50);
    ui->tabWidget->setCornerWidget(addTabPushButton, Qt::TopRightCorner);
    QObject::connect(addTabPushButton,SIGNAL(clicked()),this, SLOT(addTab()));
    QObject::connect(ui->tabWidget, &QTabWidget::tabBarDoubleClicked, this, &MainWindow::ChangeTitleTab);
    ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
}

MainWindow::~MainWindow()
{
    delete ui;
    massCoordinatesTableModel.clear();
    delete dbManager;
}

void MainWindow::on_savePushButton_clicked()
{
    int index = ui->tabWidget->currentIndex();
    massCoordinatesTableModel[index]->saveModelInDatabase();
}

void MainWindow::on_loadPushButton_clicked()
{
    int index = ui->tabWidget->currentIndex();
    massCoordinatesTableModel[index]->loadModelInDatabase();
    massTableView[index]->setFocus();
}

void MainWindow::on_deletePushButton_clicked()
{
    if(kits.count() > 1){
        int index = ui->tabWidget->currentIndex();
        if(index > 1)
        {
            ui->tabWidget->setCurrentIndex(index - 1);
        }
        massCoordinatesTableModel[index]->deleteModelInDatabase();
        massCoordinatesTableModel.removeAt(index);
        massTableView.removeAt(index);
        ui->tabWidget->removeTab(index);
        kits.removeAt(index);
    }
}

void MainWindow::initCoordinatesTabs()
{
    ui->tabWidget->clear();
    kits = dbManager->GetAllKits();
    for (auto kit : kits)
    {
        createNewTab(kit);
    }
    ui->tabWidget->setCurrentIndex(0);
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if (index >= 0)
    {
        if(massTableView.count() > 0 &&
           massCoordinatesTableModel.count() > 0)
        {
            massTableView[index]->setModel(massCoordinatesTableModel[index]);
            massCoordinatesTableModel[index]->DrawGraphic();
        }
    }
}

void MainWindow::addTab()
{
    bool ok;
    QString newName = QInputDialog::getText(this, tr("Новый набор"),
                                         tr("Введите новое название набора:"), QLineEdit::Normal,
                                         "", &ok);
    if(ok)
    {
        Kit kit{};
        kit.name = newName;
        kit.KitID = dbManager->GetMaxKitID() + 1;
        kit.massX = QList<QString>(10);
        kit.massY = QList<QString>(10);
        QColor color = QColorDialog::getColor(Qt::yellow, this);
        if(color.isValid())
        {
            QString col = color.toRgb().name();
            kit.color = color.toRgb().name();
        }
        else
        {
            kit.color = QColor(Qt::black).toRgb().name();
        }
        kits.append(kit);
        createNewTab(kit);
    }
}

void MainWindow::ChangeTitleTab(int index)
{
    bool ok;
    QString newName = QInputDialog::getText(this, tr("Переименование набора"),
                                         tr("Введите новое название набора:"), QLineEdit::Normal,
                                         "", &ok);
    if(ok)
    {
        ui->tabWidget->setTabText(index, newName);
        massCoordinatesTableModel[index]->updateTitleKitInDatabase(newName);
    }
}

void MainWindow::createNewTab(Kit kit)
{
    CoordinatesTableModel *model = new CoordinatesTableModel(&kit);
    GraphicDrawer *drawer = new GraphicDrawer(ui->widget);
    drawer->SetGraphicColor(kit.color);
    model->setDrawer(drawer);
    massCoordinatesTableModel.append(model);
    QTableView *tv = new QTableView();
    tv->setModel(model);
    tv->horizontalHeader()->hide();
    tv->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    tv->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tabWidget->addTab(tv, kit.name);
    massTableView.append(tv);
}
