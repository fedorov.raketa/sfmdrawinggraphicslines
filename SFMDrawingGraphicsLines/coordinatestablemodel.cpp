#include "coordinatestablemodel.h"

CoordinatesTableModel::CoordinatesTableModel(Kit* kit, QObject *parent)
    : QAbstractTableModel(parent)
{
    this->kit = *kit;
    dbManager = DataBaseManager::getInstance();
}

QVariant CoordinatesTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
        if (orientation == Qt::Vertical) {
            switch (section) {
            case 0:
                return "X";
            case 1:
                return "Y";
            default:
                return QVariant();
            }
        }
    return QVariant();
}

bool CoordinatesTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int CoordinatesTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 2;
}

int CoordinatesTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    if(kit.massX.count() > 0)
        return kit.massX.count() + 1;
    else
        return 10;
    if(kit.massY.count() > 0)
        return kit.massY.count() + 1;
    else
        return 10;
}

QVariant CoordinatesTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        if (index.row() >= 0 && index.row() < 2) {
            switch(index.row())
            {
            case 0:
                if(index.column() >= 0 && index.column() < kit.massX.count())
                {
                    return kit.massX[index.column()];
                }
                else
                {
                    return QVariant();
                }
            case 1:
                if(index.column() >= 0 && index.column() < kit.massY.count())
                {
                    return kit.massY[index.column()];
                }
                else
                {
                    return QVariant();
                }
            default: return QVariant();
            }
        }
    }
    return QVariant();
}

bool CoordinatesTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        if(role == Qt::EditRole)
        {
            switch(index.row())
            {
                case 0:
                {
                    if(index.column() < kit.massX.count())
                        kit.massX.replace(index.column(), value.toString());
                    else
                    {
                        kit.massX.append(value.toString());
                    }
                    if(index.column() == kit.massX.count() - 1){
                        insertColumns(index.column() + 1, 1);
                    }
                    break;
                }
                case 1:
                {
                    if(index.column() < kit.massY.count())
                    {
                        kit.massY.replace(index.column(), value.toString());
                    }
                    else
                    {
                        kit.massY.append(value.toString());
                    }                    
                    break;
                }
            }            
        }        
        emit dataChanged(index, index, QVector<int>() << role);
        DrawGraphic();
        return true;
    }
    return false;
}

Qt::ItemFlags CoordinatesTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

bool CoordinatesTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    endInsertRows();
    return false;
}

bool CoordinatesTableModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    kit.massX.append("");
    kit.massY.append("");
    endInsertColumns();
    return true;
}

bool CoordinatesTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    endRemoveRows();
    return false;
}

bool CoordinatesTableModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endRemoveColumns();
    return false;
}

void CoordinatesTableModel::setDrawer(GraphicDrawer *value)
{
    if(value != nullptr){
        drawer = value;
        DrawGraphic();
    }
}

void CoordinatesTableModel::setKitName(const QString &value)
{
    kitName = value;
}

void CoordinatesTableModel::setColor(const QColor &value)
{
    color = value;
}

void CoordinatesTableModel::saveModelInDatabase()
{
    dbManager->saveKit(kit);
    DrawGraphic();
}

void CoordinatesTableModel::loadModelInDatabase()
{
    kit = dbManager->loadKit(kit.KitID);
    DrawGraphic();
}

void CoordinatesTableModel::deleteModelInDatabase()
{
    dbManager->deleteKit(kit.KitID);
}

void CoordinatesTableModel::DrawGraphic() const
{
    if(kit.massX.isEmpty() || kit.massY.isEmpty())
        return;
    QVector<double> listX;
    for(auto item : kit.massX){
        if(!item.isEmpty())
            listX.append(item.toDouble());
    }

    QVector<double> listY;
    for(auto item : kit.massY){
        if(!item.isEmpty())
            listY.append(item.toDouble());
    }
    drawer->SetKitName(kit.name);
    drawer->SetGraphicColor(kit.color);
    drawer->DrawGraphic(listX, listY);
}

void CoordinatesTableModel::updateTitleKitInDatabase(QString newName)
{
    dbManager->updateTitleKit(newName, kit.name);
    kit.name = newName;
}
