#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QRect>
#include "databasemanager.h"
#include "coordinatestablemodel.h"
#include "graphicdrawer.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_savePushButton_clicked();
    void on_loadPushButton_clicked();
    void on_deletePushButton_clicked();
    void on_tabWidget_currentChanged(int index);

    void addTab();
    void ChangeTitleTab(int index);

private:
    Ui::MainWindow *ui;
    CoordinatesTableModel* coordinatesTableModel;
    QList<CoordinatesTableModel*> massCoordinatesTableModel;
    QList<QTableView*> massTableView;
    QList<Kit> kits;
    DataBaseManager* dbManager;
    void initCoordinatesTabs();
    void DrawLine(double x1, double y1, double x2, double y2);
    void createNewTab(Kit kit);
};
#endif // MAINWINDOW_H
