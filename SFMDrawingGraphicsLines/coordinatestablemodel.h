#ifndef COORDINATESTABLEMODEL_H
#define COORDINATESTABLEMODEL_H

#include <QAbstractTableModel>
#include <QColor>
#include "graphicdrawer.h"
#include "databasemanager.h"

class CoordinatesTableModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  explicit CoordinatesTableModel(Kit* kit, QObject *parent = nullptr);

  // Header:
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

  // Basic functionality:
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  // Add data:
  bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
  bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

  // Remove data:
  bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
  bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

  //draw graphic
  void setDrawer(GraphicDrawer *value);
  void setKitName(const QString &value);
  void setColor(const QColor &value);

  //for database
  void saveModelInDatabase();
  void loadModelInDatabase();
  void deleteModelInDatabase();

  void DrawGraphic() const;
  void updateTitleKitInDatabase(QString newName);
private:
  DataBaseManager* dbManager;
  GraphicDrawer* drawer;
  QString kitName;
  QColor color;
  Kit kit;
  QList<QString> xs;
  QList<QString> ys;  
};

#endif // COORDINATESTABLEMODEL_H
