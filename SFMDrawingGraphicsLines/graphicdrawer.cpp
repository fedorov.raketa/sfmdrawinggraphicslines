#include "graphicdrawer.h"

GraphicDrawer::GraphicDrawer(QCustomPlot* customPlotter) : plot(customPlotter)
{
    plot->setInteraction(QCP::iRangeZoom, true);
    plot->setInteraction(QCP::iRangeDrag, true);
    plot->setLocale(QLocale(QLocale::Russian, QLocale::RussianFederation));
}

void GraphicDrawer::DrawGraphic(QVector<double>& listX, QVector<double>& listY)
{    
    if(listX.isEmpty() || listY.isEmpty())
        return;

    plot->clearGraphs();
    auto mm = std::minmax_element(listX.begin(), listX.end());
    plot->xAxis->setRange(*mm.first-1,*mm.second+1);
    mm = std::minmax_element(listY.begin(), listY.end());
    plot->yAxis->setRange(*mm.first-1,*mm.second+1);

    plot->addGraph(0);
    plot->graph(0)->setPen(color);
    plot->graph(0)->setLineStyle(QCPGraph::lsLine);
    SetMarkerStyle();
    plot->graph(0)->addData(listX, listY);
    DrawLegend();
    plot->replot();
}

void GraphicDrawer::SetMarkerStyle()
{
    QCPScatterStyle scatterStyle(QCPScatterStyle::ssDiamond);
    scatterStyle.setBrush(QBrush(color));
    scatterStyle.setSize(10);
    plot->graph(0)->setScatterStyle(scatterStyle);
}

void GraphicDrawer::DrawLegend()
{
    plot->graph(0)->setName(kitName);
    plot->legend->setVisible(true);
}

void GraphicDrawer::SetGraphicColor(QColor color)
{
    this->color = color;
}

void GraphicDrawer::SetKitName(const QString &value)
{
    kitName = value;
}
