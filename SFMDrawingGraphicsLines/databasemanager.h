#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QtSql>

struct Kit{
    int KitID;
    QString name;
    QList<QString> massX;
    QList<QString> massY;
    QColor color;
};

class DataBaseManager
{
public:
    DataBaseManager();
    ~DataBaseManager();
    static DataBaseManager* instance;
    static DataBaseManager* getInstance();

    QList<Kit> GetAllKits();
    int GetMaxKitID();

    void saveKit(Kit& kit);
    Kit loadKit(int kitID);
    void deleteKit(int KitID);
    void updateTitleKit(QString newName, QString oldName);

private:
    QSqlDatabase sdb;
};


#endif // DATABASEMANAGER_H
