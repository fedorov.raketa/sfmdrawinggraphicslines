#include "databasemanager.h"

#include <QMessageBox>

DataBaseManager* DataBaseManager::instance = 0;

DataBaseManager::DataBaseManager()
{
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("DataBaseCoordinates.db");
    sdb.open();
}
DataBaseManager::~DataBaseManager()
{
    sdb.close();
}

 DataBaseManager* DataBaseManager::getInstance() {
    if(!instance)
        instance = new DataBaseManager();
    return instance;
}

QList<Kit> DataBaseManager::GetAllKits()
{
    QList<int> listKitID;
    QList<Kit> kitList;
    if(!sdb.isOpen())
    {
        sdb.open();
    }
    if(sdb.isOpen())
    {
        QSqlQuery queryKits("SELECT KitID FROM Kits");

        while (queryKits.next())
        {
            listKitID.append(queryKits.value(0).toInt());
        }

    }
    for(auto id : listKitID){
        Kit kit = loadKit(id);
        kitList.append(kit);
    }
    return kitList;
}

int DataBaseManager::GetMaxKitID()
{
    int maxID = 0;
    if(!sdb.isOpen())
    {
        sdb.open();
    }
    if(sdb.isOpen())
    {
        QSqlQuery queryMaxID("SELECT MAX(KitID) FROM Kits");
        queryMaxID.next();
        maxID = queryMaxID.value(0).toInt();
    }
    return maxID;
}

void DataBaseManager::saveKit(Kit& kit)
{
    if(!sdb.isOpen())
    {
        sdb.open();
    }
    if(sdb.isOpen())
    {
        //delete from Kits
        deleteKit(kit.KitID);

        //insert in Kits
        QSqlQuery queryKit;
        queryKit.prepare("INSERT INTO Kits(KitID, Name, Color) VALUES(:id, :Name, :Color)");
        queryKit.bindValue(":id", kit.KitID);
        queryKit.bindValue(":Name", kit.name);
        queryKit.bindValue(":Color", kit.color);
        if(queryKit.exec())
        {
            sdb.commit();
        }

        //inser in coordinates
        for(int i = 0; i < kit.massX.count(); ++i)
        {
            if(kit.massX[i].isEmpty() || kit.massY[i].isEmpty())
                continue;
            QSqlQuery queryCoordinates;
            queryCoordinates.prepare("INSERT INTO Coordinates(KitID, X, Y) VALUES(:KitID, :x, :y)");
            queryCoordinates.bindValue(":KitID", kit.KitID);
            queryCoordinates.bindValue(":x", kit.massX[i]);
            queryCoordinates.bindValue(":y", kit.massY[i]);
            if(queryCoordinates.exec())
            {
                sdb.commit();
            }
        }
    }
}

Kit DataBaseManager::loadKit(int kitId)
{
    Kit* kit = new Kit();
    if(!sdb.isOpen())
    {
        sdb.open();
    }
    QSqlQuery queryKit;
    queryKit.prepare("SELECT KitId, Name, Color FROM Kits WHERE KitID=:id");
    queryKit.bindValue(":id", kitId);
    if(queryKit.exec())
    {
        queryKit.next();
        kit->KitID = queryKit.value(0).toInt();
        kit->name = queryKit.value(1).toString();
        kit->color.setNamedColor(queryKit.value(2).toString());

        QSqlQuery queryCoordinates;
        queryCoordinates.prepare("SELECT X,Y FROM COORDINATES WHERE KitID=:id");
        queryCoordinates.bindValue(":id", kit->KitID);
        if(!queryCoordinates.exec())
            QMessageBox::warning(NULL, "Database Error", queryCoordinates.lastError().text());
        while(queryCoordinates.next())
        {
            kit->massX.append(queryCoordinates.value(0).toString());
            kit->massY.append(queryCoordinates.value(1).toString());
        }
    }
    return *kit;
}

void DataBaseManager::deleteKit(int KitID)
{
    if(!sdb.isOpen())
    {
        sdb.open();
    }
    QSqlQuery queryDelete;
    queryDelete.prepare("DELETE FROM Kits WHERE KitID=:id;");
    queryDelete.bindValue(":id", KitID);
    if(queryDelete.exec())
    {
        sdb.commit();
    }
    queryDelete.prepare("DELETE FROM Coordinates WHERE KitID=:id");
    queryDelete.bindValue(":id", KitID);
    if(queryDelete.exec())
    {
        sdb.commit();
    }
}

void DataBaseManager::updateTitleKit(QString newName, QString oldName)
{
    if(!sdb.isOpen())
    {
        sdb.open();
    }
    QSqlQuery query;
    query.prepare("UPDATE Kits SET Name=:newName WHERE Name=:oldName");
    query.bindValue(":newName", newName);
    query.bindValue(":oldName", oldName);
    if(query.exec())
    {
        sdb.commit();
    }
}

