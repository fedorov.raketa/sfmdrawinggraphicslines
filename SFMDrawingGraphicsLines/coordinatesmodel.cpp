#include "coordinatesmodel.h"

CoordinatesModel::CoordinatesModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    loadData();
}

QVariant CoordinatesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    if(role == Qt::DisplayRole){
        if(orientation == Qt::Vertical)
        {
            switch(section)
            {
            case 0: return "X";
            case 1: return "Y";
              default: QVariant();
            }
        }
    }
    return QVariant();
}

bool CoordinatesModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int CoordinatesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 2;
}

int CoordinatesModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if(_kits.count() > 0)
    {
        return _kits.count();
    }
    else
    {
        return 0;
    }
}

QModelIndex CoordinatesModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if(row >= 0 && row < 2 && column >= 0 && column < 10){
        createIndex(row, column);
    }
    return QModelIndex();
}

QVariant CoordinatesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        if(_kits.count() == 0)
            return QVariant();
        for(int i = 0; i < _kits.count(); i++)
        {
            switch (index.column()) {
                case 0: return "X";
                case 1: return "1";
                case 2: return "2";
            default: return _kits[i].X;
            }
        }
    }
    return QVariant();
}

bool CoordinatesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!        
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags CoordinatesModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

void CoordinatesModel::loadData()
{
    for(int i = 0; i < 10; ++i){
        Kit kit;
        kit.X = i + 1;
        kit.Y = i + 10;
        _kits.push_back(kit);
    }
    int x = 0;
}
